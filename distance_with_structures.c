#include<stdio.h>
#include<math.h>
struct points
{
    float x,y;
};
float point1(struct points m, struct points n)
{
     return (m.x - n.x)*(m.x - n.x);
}
float point2(struct points m, struct points n)
{
     return (m.y - n.y)*(m.y - n.y);
}
float distance(struct points m, struct points n)
{
     return sqrt(point1(m,n) + point2(m,n));
}
float output(struct points m, struct points n)
{
     printf("The distance between two points is: %0.2f\n", distance(m,n));
}
int main()
{
     struct points m,n;
      printf("Coordinates of Point1: ");
      scanf("%f%f",&m.x,&m.y);
      printf("Coordinates of Point2: ");
      scanf("%f%f",&n.x,&n.y);
      printf("Distance between point1(%.2f, %.2f) and point2(%.2f, %.2f) is: \n",m.x,m.y,n.x,n.y);
      output(m,n);
      return 0;
}

//Suraj Bhadra - ENG19CS0325