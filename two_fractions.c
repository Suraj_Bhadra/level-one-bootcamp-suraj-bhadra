#include<stdio.h>
struct fraction
{
    int num1,denom1,num2,denom2;
};

int lcm(struct fraction f1, struct fraction f2)
{
    int max=0;
    if(f1.denom1>f2.denom2)
    {
        max=f1.denom1;
    }
    else
    {
        max=f2.denom2;
    }
    while(1)
    {
        if (max%f1.denom1 == 0 && max%f2.denom2 == 0)
        {
            break;
        }
        max++;
    }  
return max;
}

int num1(struct fraction n1, struct fraction f1, struct fraction f2)
{
    n1.num1 *= lcm(f1,f2)/f1.denom1;
}

int num2(struct fraction n2, struct fraction f1, struct fraction f2)
{
    n2.num2 *= lcm(f1,f2)/f2.denom2;
}

int output(struct fraction n1, struct fraction f1, struct fraction n2, struct fraction f2)
{
    return num1(n1,f1,f2) + num2(n2,f1,f2);
}

int gcd(int num, int denom)
{
    if (num == 0)
    {
        return denom;
    }
    else if (denom == 0)
    {
        return num;
    }
    else if (num > denom)
    {
        return gcd(num-denom, denom);
    }
    else
    {
        return gcd(num, denom-num);
    }
}

int main()
{
    struct fraction n1,f1,n2,f2;
    printf("Enter the value of Numerator 1:  ");
    scanf("%d",&n1.num1);
    printf("Enter the value of Denominator 1: ");
    scanf("%d",&f1.denom1);
    
    printf("Enter the value of Numerator 2: ");
    scanf("%d",&n2.num2);
    printf("Enter the value of Denominator 2: ");
    scanf("%d",&f2.denom2);
    
    int num = output(n1,f1,n2,f2);
    int denom = lcm(f1,f2);
     
    printf("Sum of two fractions %d/%d and %d/%d is: \n", n1.num1, f1.denom1, n2.num2, f2.denom2);
    printf("%d/%d",output(n1,f1,n2,f2)/gcd(num,denom), lcm(f1,f2)/gcd(num,denom));
    return 0;
}
//Suraj Bhadra - ENG19CS0325